# Glass Echidna Tabletop Simulator Libraries

Moved to [Github](https://github.com/Benjamin-Dobell/ge_tts).

## Already cloned?

If you've already cloned this repository locally, you can update the origin to https://github.com/Benjamin-Dobell/ge_tts.git

If you used a UI to clone the repository, it should be able to update the origin. Otherwise, you can do it from command line with:

```bash
git remote set-url origin https://github.com/Benjamin-Dobell/ge_tts.git
```

## Discord

If you'd like to discuss ge_tts, you can do so on the [TTS Community Discord](https://discord.gg/YwD22SM).

